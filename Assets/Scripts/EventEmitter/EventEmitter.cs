using UnityEngine;
using System;

public class EventEmitter : MonoBehaviour
{
    public static EventEmitter Instance;
    public event Action<int> OnZoneDetection;

    private void Awake()
    {
        if (Instance != null && Instance != this)
        {
            Destroy(this);
            return;
        }
        Instance = this;
    }

    public void EmitZoneDetection(int temperature)
    {
        OnZoneDetection?.Invoke(temperature);
    }
}
