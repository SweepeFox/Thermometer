using UnityEngine;

[RequireComponent(typeof(Collider))]
public class Draggable : MonoBehaviour
{
    private Plane _dragPlane;
    private Vector3 _offset;
    private Camera _mainCamera;

    private void Awake()
    {
        _mainCamera = Camera.main;
    }

    private void OnMouseDown()
    {

        _dragPlane = new Plane(_mainCamera.transform.forward, transform.position);
        Ray ray = _mainCamera.ScreenPointToRay(Input.mousePosition);
        float planeDist;
        _dragPlane.Raycast(ray, out planeDist);
        _offset = transform.position - ray.GetPoint(planeDist);
    }

    private void OnMouseDrag()
    {
        Ray ray = _mainCamera.ScreenPointToRay(Input.mousePosition);
        float planeDist;
        _dragPlane.Raycast(ray, out planeDist);
        transform.position = ray.GetPoint(planeDist) + _offset;
    }
}