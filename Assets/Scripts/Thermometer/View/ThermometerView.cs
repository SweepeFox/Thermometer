using UnityEngine.UI;

public class ThermometerView
{
    private readonly ThermometerModel _model;
    private readonly Text _thermometerText;

    public ThermometerView(ThermometerModel model, Text thermometerText)
    {
        _model = model;
        _thermometerText = thermometerText;
        _model.OnUpdated += OnModelUpdated;
    }

    private void OnModelUpdated()
    {
        _thermometerText.text = _model.Temperature.ToString();
    }

    public void OnDestroy()
    {
        _model.OnUpdated -= OnModelUpdated;
    }
}