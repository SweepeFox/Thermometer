using UnityEngine;
using UnityEngine.UI;

public class Thermometer : Draggable
{
    [SerializeField] private Text _thermometerText;
    [SerializeField] private LayerMask _zoneLayer;
    private ThermometerModel _model;
    private ThermometerView _view;
    private ThermometerController _controller;

    private void Start()
    {
        _model = new ThermometerModel();
        _view = new ThermometerView(_model, _thermometerText);
        _controller = new ThermometerController(transform, _zoneLayer);
    }

    private void Update()
    {
        _model.Update();
    }

    private void FixedUpdate()
    {
        _controller.DetectZoneHandling();
    }

    private void OnDestroy()
    {
        _model.OnDestroy();
        _view.OnDestroy();
    }
}