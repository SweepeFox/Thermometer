using System;
using UnityEngine;

public class ThermometerModel
{
    public event Action OnUpdated;
    public int Temperature => _currentTemperature;
    private int _currentTemperature;
    private int _newTemperature;

    private readonly float _changeTemperatureValueSpeed = 0.5f;
    private float _updateTime = 0.0f;

    public ThermometerModel()
    {
        EventEmitter.Instance.OnZoneDetection += OnZoneDetection;
    }

    private void OnZoneDetection(int temperature)
    {
        if (_newTemperature != temperature)
        {
            _newTemperature = temperature;
        }
    }

    public void Update()
    {
        if (_currentTemperature == _newTemperature) return;
        _currentTemperature = (int)Mathf.Lerp(_currentTemperature, _newTemperature, _updateTime);
        _updateTime += _changeTemperatureValueSpeed * Time.deltaTime;
        if (_updateTime > 1)
        {
            _updateTime = 0;
            _currentTemperature = _newTemperature;
        }
        OnUpdated?.Invoke();
    }

    public void OnDestroy()
    {
        EventEmitter.Instance.OnZoneDetection -= OnZoneDetection;
    }
}