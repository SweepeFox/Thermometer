using UnityEngine;

public class ThermometerController
{
    private readonly Transform _thermometerTransform;
    private readonly LayerMask _zoneLayer;

    public ThermometerController(Transform thermometerTransform, LayerMask zoneLayer)
    {
        _thermometerTransform = thermometerTransform;
        _zoneLayer = zoneLayer;
    }

    public void DetectZoneHandling()
    {
        RaycastHit hit;
        if (Physics.Raycast(_thermometerTransform.position, _thermometerTransform.TransformDirection(Vector3.down), out hit, Mathf.Infinity, _zoneLayer))
        {
            hit.transform.TryGetComponent(out Zone zone);
            if (zone)
            {
                EventEmitter.Instance.EmitZoneDetection(zone.Temperature);
            }
        }
    }
}